<?php

namespace Drupal\clear_field\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for Clear Field module.
 */
class ClearFieldSettingsForm extends ConfigFormBase {


  const SETTINGS = 'clear_field.settings';

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clear_field_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * ClearFieldSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   Module Handler.
   *   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandler $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('module_handler')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form = parent::buildForm($form, $form_state);

    $types = [
      'cf_fa_generic' => $this->t('Font Awesome  for all fields generic'),
      'cf_x_text' => $this->t('Text along with X for all'),
    ];
    // @todo to convert it into boolean and make sure #states work
    $form['clear_button_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clear Field'),
      '#default_value' => $config->get('clear_button_status'),
    ];

    $form['clear_button_type'] = [
      '#type' => 'select',
      '#options' => $types,
      '#title' => $this->t('Type of Button'),
      '#default_value' => $config->get('clear_button_type'),
      '#states' => [
        'visible' => [
          ':input[name="clear_button_status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['clear_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text for the icon'),
      '#default_value' => $config->get('clear_button_text'),
      '#states' => [
        'visible' => [
          ':input[name="clear_button_status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['clear_button_tooltip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text for the icon tooltip'),
      '#default_value' => $config->get('clear_button_tooltip'),
      '#states' => [
        'visible' => [
          ':input[name="clear_button_status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['clear_button_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#description' => $this->t('Add input element classes separated by space for which you need the cross button to be visible.'),
      '#default_value' => $config->get('clear_button_classes'),
      '#states' => [
        'visible' => [
          ':input[name="clear_button_status"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (!$this->moduleHandler->moduleExists('fontawesome') && $this->config('clear_field.settings')->get('clear_button_type') == 'cf_fa_generic') {
      $this->messenger->addMessage($this->t('You need to enable <a href=":fontawesome">FontAwesome</a> module to display fontawesome icons.', [':fontawesome' => 'https://www.drupal.org/project/fontawesome']), 'error');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('clear_button_type') === 'cf_fa_generic') {
      if (!$this->moduleHandler->moduleExists('fontawesome')) {
        $form_state->setErrorByName('clear_button_type', $this->t('You need to enable <a href=":fontawesome">FontAwesome</a> module to display fontawesome icons', [':fontawesome' => 'https://www.drupal.org/project/fontawesome']));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
    $values = $form_state->getValues();
    $this->config('clear_field.settings')
      ->set('clear_button_status', $values['clear_button_status'])
      ->save();
    $this->config('clear_field.settings')
      ->set('clear_button_type', $values['clear_button_type'])
      ->save();
    $this->config('clear_field.settings')
      ->set('clear_button_classes', $values['clear_button_classes'])
      ->save();
    $this->config('clear_field.settings')
      ->set('clear_button_text', $values['clear_button_text'])
      ->save();
    $this->config('clear_field.settings')
      ->set('clear_button_tooltip', $values['clear_button_tooltip'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
