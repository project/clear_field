# clear_field

INTRODUCTION
------------

The Clear field values module helps display the cross button
besides input fields of type text like textfield, autocomplete etc.


REQUIREMENTS
------------

This module has no dependencies.
If you need to use fontawesome icons for cross button.
Please enable the fontawesome module: https://www.drupal.org/project/fontawesome  and then set dropdown value "Font Awesome for all generic fields"

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
------------
To configure the Clear Field, you need to
go to /admin/config/clear-field/settings
Steps
1. Enable Clear Field by tick marking.
2. Select Type of Button. Is it ?
     a. Simple X
     b. Fontawesome for all generic fields
3. You can add text and tooltip which you need to add along with icon
   or keep it empty if not needed.
