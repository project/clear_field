<?php

/**
 * @file
 * Clear Field Values.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function clear_field_help($route_name, RouteMatchInterface $route_match) {

  switch ($route_name) {

    // Main module help for the phone_registration module.
    case 'help.page.clear_field':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module provides an "x" icon to clear text entered in textfields or input of type text') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function clear_field_theme($existing, $type, $theme, $path) {
  return [
    'clear_field' => [
      'variables' => [
        'span_class' => NULL,
        'a_class' => NULL,
        'fa_icon' => NULL,
        'fa_enabled' => NULL,
        'text_value' => NULL,
        'text_tooltip' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_form_alter().
 */
function clear_field_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('clear_field.settings');
  $exists = \Drupal::service('module_handler')->moduleExists('fontawesome');
  $clear_field = [
    '#theme' => 'clear_field',
    '#span_class' => 'span-class',
    '#a_class' => 'a-class',
    '#fa_icon' => ($config->get('clear_button_type') == 'cf_fa_generic'  && $exists) ?? TRUE ,
    '#text_value' => $config->get('clear_button_text'),
    '#text_tooltip' => $config->get('clear_button_tooltip'),
  ];
  // Render the template to HTML.
  $clear_field_content = \Drupal::service('renderer')->render($clear_field);
  $form['#attached']['drupalSettings']['clear_field']['close_button'] = $clear_field_content;
  $form['#attached']['drupalSettings']['clear_field']['span_class'] = $clear_field['#span_class'];

  $form['#attached']['drupalSettings']['clear_field']['form_text_class'] = ['form-text', 'form-email'];

}
